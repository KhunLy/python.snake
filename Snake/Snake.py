from console import *
import time
from threading import Thread

class getchThread(Thread):
    def __init__(self, object):
        Thread.__init__(self)
        self.object = object

    def run(self):
        while True:
            dir = getch()
            if dir == b'M':
                self.object.direction = 1
            elif dir == b'K':
                self.object.direction = 3
            elif dir == b'H':
                self.object.direction = 0
            elif dir == b'P':
                self.object.direction = 2


class Snake:
    def __init__(self):
        self.body = [(0,0),(1,0),(2,0)]
        # 0 -> North, 1 -> East, 2 -> South, 3 -> West
        self.direction = 2

    def display(self):
        for part in self.body:
            print_at(part[1], part[0], '0')

    def unDisplay(self):
        for part in self.body:
            print_at(part[1], part[0], ' ')

    def update_direction(self, value):
        direction = value
    
    def move(self):
        for i in range(len(self.body) - 1):
            self.body[i] = self.body[i+1]
        if self.direction == 0:
            self.body[len(self.body) - 1] = (self.body[len(self.body) - 1][0], self.body[len(self.body) - 1][1] - 1)
        if self.direction == 2:
            self.body[len(self.body) - 1] = (self.body[len(self.body) - 1][0], self.body[len(self.body) - 1][1] + 1)
        if self.direction == 1:
            self.body[len(self.body) - 1] = (self.body[len(self.body) - 1][0] + 1, self.body[len(self.body) - 1][1])
        if self.direction == 3:
            self.body[len(self.body) - 1] = (self.body[len(self.body) - 1][0] - 1, self.body[len(self.body) - 1][1])

snake = Snake()
thread = getchThread(snake)
thread.start()
snake.display()
while True:
    time.sleep(0.2)
    snake.unDisplay()
    snake.move()
    snake.display()